region = "us-east-1"

domain                = "staboss.com"
subdomain             = "test."
grafana_subdomain     = "grafana-test."
monitoring_subdomain  = "monitoring-test."
server_node_subdomain = "server-node-test."

private_subnet_count = 2
public_subnet_count  = 2
vpc_cidr_block       = "10.0.0.0/16"

key_name    = "test"
environment = "test"

tags = {
  Terraform   = "true"
  Environment = "test"
}

zone_id = "Z01023043EM9MP9SXVE3Z"
