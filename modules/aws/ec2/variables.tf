variable "ami" {}
variable "tags" {}
variable "key_name" {}
variable "subnet_id" {}
variable "instance_type" {}
variable "vpc_security_group_ids" {}

variable "eip_enabled" {
  default = false
}
